import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CitiesScanner {

    private String filename;

    private Scanner scanner;

    public CitiesScanner(String filename) {
        this.filename = filename;
    }

    public void open() {
        try {
            FileInputStream fileInputStream = new FileInputStream(filename);
            this.scanner = new Scanner(fileInputStream).useDelimiter(";");
        } catch (FileNotFoundException e) {
            System.err.println("Ошибка," + e.getMessage());
        }
    }

    public City nextCity() {
        if (scanner.hasNext()) {
            Long id = scanner.nextLong();
            String name = scanner.next();
            String region = scanner.next();
            String district = scanner.next();
            Long population = scanner.nextLong();
            String foundation = scanner.nextLine().replaceAll(";", "");
            return new City(id, name, region, district, population, foundation);
        } else {
            return null;
        }
    }
}
