import org.w3c.dom.ls.LSOutput;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;

public class Main {
    public static void main(String[] args) {
        CitiesScanner scanner = new CitiesScanner("city_ru.txt");
        scanner.open();

        CitiesRepository citiesRepository = new CitiesRepository(scanner);

        List<City> cities = citiesRepository.findAll();

//        System.out.println(cities);
//        sortByName(cities);
//        sortByNameAndDistrict(cities);
//        findCityWithTheLargestPopulation(cities);
        findTheNumberOfCitiesInTheRegion(cities);
    }

    private static void sortByName(List<City> cities) {
        cities.sort((o1, o2) -> o1.getName().compareToIgnoreCase(o2.getName()));
        System.out.println(cities);
    }

    private static void sortByNameAndDistrict(List<City> cities) {
        cities.sort(Comparator.comparing(City::getDistrict).thenComparing(City::getName));
        System.out.println(cities);
    }

    private static void findCityWithTheLargestPopulation(List<City> cities) {
        Object[] citiesArray = cities.toArray();

        Long maxPopulation = 0L;
        Long indexOfMax = 0L;

        for(Object city : citiesArray) {
            City max = Collections.max(cities, Comparator.comparingLong(City::getPopulation).thenComparing(City::getId));
            maxPopulation = max.getPopulation();
            indexOfMax = max.getId();
        }
        System.out.println("[" + indexOfMax + "] = " + maxPopulation);
    }

    private static void findTheNumberOfCitiesInTheRegion(List<City> cities) {
        System.out.println(cities
                .stream()
                .collect(Collectors.groupingBy(City::getRegion, counting())));
    }
}
