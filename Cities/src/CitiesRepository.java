import java.util.ArrayList;
import java.util.List;

public class CitiesRepository {

    private CitiesScanner citiesScanner;

    public CitiesRepository(CitiesScanner citiesScanner) {
        this.citiesScanner = citiesScanner;
    }

    public List<City> findAll() {
        List<City> cities = new ArrayList<>();

        City current = null;

        try {
            current = citiesScanner.nextCity();
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }

        while (current != null) {
            cities.add(current);

            try {
                current = citiesScanner.nextCity();
            } catch (Exception e) {
                throw new IllegalArgumentException(e);
            }
        }
        return cities;
    }
}
